import 'package:flutter/material.dart';
import 'package:sinpe_movil/src/models/contact_model.dart';

/// selection dialog used for selection of the country code
class FavoritoDialogo extends StatefulWidget {
  final List<Contacto> elements;
  final bool showCountryOnly;
  final InputDecoration searchDecoration;
  final TextStyle searchStyle;
  final WidgetBuilder emptySearchBuilder;

  /// elements passed as favorite
  final List<Contacto> favoriteElements;

  FavoritoDialogo(this.elements, this.favoriteElements, {
    Key key,
    this.showCountryOnly,
    this.emptySearchBuilder,
    InputDecoration searchDecoration = const InputDecoration(),
    this.searchStyle,
  }) :
        assert(searchDecoration != null, 'searchDecoration must not be null!'),
        this.searchDecoration = searchDecoration.copyWith(prefixIcon: Icon(Icons.search)),
        super(key: key);

  @override
  _FavoritoDialogoState createState() => _FavoritoDialogoState();
}

class _FavoritoDialogoState extends State<FavoritoDialogo> {
  /// this is useful for filtering purpose
  List<Contacto> filteredElements;

  @override
  Widget build(BuildContext context) => SimpleDialog(
    title: Column(
      children: <Widget>[
        TextField(
          style: widget.searchStyle,
          decoration: widget.searchDecoration,
          onChanged: _filterElements,
        ),
      ],
    ),
    children: [
      Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: ListView(
              children: [
                widget.favoriteElements.isEmpty
                    ? const DecoratedBox(decoration: BoxDecoration())
                    : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[]
                      ..addAll(widget.favoriteElements
                          .map(
                            (f) => SimpleDialogOption(
                          child: _buildOption(f,context),
                          onPressed: () {
                            _selectItem(f);
                          },
                        ),
                      )
                          .toList())
                      ..add(const Divider())),
              ]..addAll(filteredElements.isEmpty
                  ? [_buildEmptySearchWidget(context)]
                  : filteredElements.map(
                      (e) => SimpleDialogOption(
                    key: Key(e.id.toString()),
                    child: _buildOption(e,context),
                    onPressed: () {
                      _selectItem(e);
                    },
                  )))
          )
      ),
    ],
  );

  Widget _buildOption(Contacto e,BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Row(
        //direction: Axis.horizontal,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[         
          SizedBox(width: 15.0),
          Expanded(
            flex: 1,
            child: ListTile(
              key: UniqueKey(),
              leading: Icon(Icons.account_circle,size: 40.0,),
              title: Text(e.nombre),
              subtitle: Text(e.telefono),
            )
          ),
        ],
      ),
    );
  }

  Widget _buildEmptySearchWidget(BuildContext context) {
    if (widget.emptySearchBuilder != null) {
      return widget.emptySearchBuilder(context);
    }

    return Center(child: Text('No hay contactos en su agenda'));
  }

  @override
  void initState() {
    filteredElements = widget.elements;
    super.initState();
  }

  void _filterElements(String s) {
    s = s.toLowerCase();
    setState(() {
      filteredElements = widget.elements
          .where((e) =>
          (e.nombre.toLowerCase().contains(s) ||
           e.telefono.contains(s)))
          .toList();
    });
  }

  void _selectItem(Contacto e) {
    Navigator.pop(context, e);
  }
}