import 'package:flutter/material.dart';

final _icons = <String, IconData>{
  'add_alert'       : Icons.add_alert,
  'accessibility'   : Icons.accessibility,
  'folder_open'     : Icons.folder_open,
  'account_balance' : Icons.account_balance,
  'send'            : Icons.send,
  'list'            : Icons.list,
  'favorite'        : Icons.star,
  'smartphone'      : Icons.smartphone
};

Icon getIcon(String nombreIcono, {Color color = Colors.blue, double tamanio = 45.0}){

  return Icon(_icons[nombreIcono], color: color,size: tamanio);
}