
import 'package:flutter/material.dart';

class FloatingButtons extends StatelessWidget {

  GestureTapCallback onpressed1;
  GestureTapCallback onpressed2;


  FloatingButtons(this.onpressed1,this.onpressed2);

  @override
  Widget build(BuildContext context) {
    return _crearBotonesFlotantes();
  }

  Widget _crearBotonesFlotantes() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        FloatingActionButton(
                    heroTag: "btnContactos",
                    onPressed: onpressed1,           
                    child: Icon(Icons.contacts),
                    backgroundColor: Color(0xFF075B9A),
        ),
         FloatingActionButton(
                    heroTag: "btnFavoritos",
                    onPressed: onpressed2,            
                    child: Icon(Icons.star),
                    backgroundColor: Color(0xFF075B9A),
        ),
      ],
    );
  }

  
}

