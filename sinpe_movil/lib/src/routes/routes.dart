import 'package:flutter/cupertino.dart';
import 'package:sinpe_movil/splash_page.dart';
import 'package:sinpe_movil/src/pages/favoritos_page.dart';
import 'package:sinpe_movil/src/pages/home_page.dart';
import 'package:sinpe_movil/src/pages/pase_dinero.dart';
import 'package:sinpe_movil/src/pages/recarga_page.dart';
import 'package:sinpe_movil/src/pages/saldo_page.dart';
import 'package:sinpe_movil/src/pages/ultimos_movs.dart';

Map<String,WidgetBuilder> getApplicationRoutes(){

    return <String, WidgetBuilder>{
            '/'     : (BuildContext context) => SplashPage(),
            'pase'  : (BuildContext context) => PaseDineroPage(),
            'ultimos'  : (BuildContext context) => UltimosMovsPage(context),
            'favoritos'  : (BuildContext context) => FavoritosPage(),
            'recarga'  : (BuildContext context) => RecargaPage(),
            'saldo'  : (BuildContext context) => SaldoPage(context),
    };

}

