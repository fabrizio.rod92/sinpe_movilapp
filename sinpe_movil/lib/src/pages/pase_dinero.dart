//import 'package:contacts_service/contacts_service.dart';
import 'dart:io';

import 'package:contact_picker/contact_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:sinpe_movil/src/models/contact_model.dart';
import 'package:sinpe_movil/src/providers/db_provider.dart';
import 'package:sinpe_movil/src/utils/buttons_custom.dart';
import 'package:sinpe_movil/src/utils/favoritos_dialog.dart';
import 'package:sinpe_movil/src/utils/floating_buttons.dart';
import 'package:sinpe_movil/src/utils/send_message_util.dart';


class PaseDineroPage extends StatefulWidget{
  String _numero ='';
  PaseDineroPage({String numero = ''}){
    _numero = numero;
  }
  @override
  _PaseDineroPage createState() => _PaseDineroPage(_numero);
}


class _PaseDineroPage extends State<PaseDineroPage> {
  final ContactPicker _contactPicker = new ContactPicker();
  final String _codigoPais = "+506";
  String _telefono = '';
  String _monto = '';
  String _concepto = '';
  _PaseDineroPage(String numero){
    _telefono = numero;
    phoneTextFieldController.text = numero;
  }

  //Contacts
  Iterable<Contact> _contacts;
  Contact _actualContact;
  ProgressDialog pr;

  List<Contacto> _contactosFavoritos;
  Contacto _contactoActual;
  TextEditingController phoneTextFieldController = TextEditingController();
  TextEditingController guestnameTextFieldController = TextEditingController();
  
  @override
  void initState() {    
    super.initState();
    
  }

  final _formPaseDinero = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {

    pr = ProgressDialog(context,type:ProgressDialogType.Normal);
    pr.style(
      message: "Cargando"
    );

    return Form(
        key: _formPaseDinero,
        child: Scaffold(                 
          appBar: AppBar(
            title: Text('Pase Dinero',style: TextStyle(color: Colors.white)),
            backgroundColor: Color(0xFF1b1e24),
            brightness: Brightness.dark,
            leading: _crearLeading(),            
          ),
          body: SafeArea(
              child: ListView(
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
                children: <Widget>[
                  _crearInputTelefono(),
                  Padding(padding: EdgeInsets.only(top: 30.0)),             
                  _crearInputMonto(),
                  Padding(padding: EdgeInsets.only(top: 30.0)), 
                  _crearInputConcepto(),
                  Padding(padding: EdgeInsets.only(top: 30.0)),   
                  ButtonCustom(
                    color: Color(0xFF075B9A),
                    heigth: 50.0,
                    txt: "Enviar",
                    ontap: () {
                      if(_formPaseDinero.currentState.validate()){
                        _enviarMensaje();
                      }          
                    },
                  ),
                  
                ],
              ),
          ), 
          bottomNavigationBar: _crearBottomNavigationBar(),
          floatingActionButton: _crearFloatingActionButtons(),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

      ),
    );
  }

  Widget _crearInputTelefono() {
    return 
        Container(
            width: MediaQuery.of(context).size.width*1,
            padding:
            EdgeInsets.only(left: 10.0, right: 10.0, top: 0.0, bottom: 0.0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
                boxShadow: [BoxShadow(blurRadius: 10.0, color: Colors.black12)]),
            child: TextFormField(
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                WhitelistingTextInputFormatter.digitsOnly
              ],
              controller: phoneTextFieldController,
              validator: (val) {
                if(val == ''){
                  return 'Por favor ingrese un número';
                }
                else if(val.length != 8){
                  return 'La longitud del número de telefono debe ser 8.';
                }
                return null;
              },
              decoration: new InputDecoration(
                  border: InputBorder.none,
                  icon: Icon(Icons.phone),
                  labelText: 'Número teléfono',
                  hintText: 'Digita el número de teléfono a transferir',
                  suffixStyle: const TextStyle(color: Colors.green)
              ),
              onChanged: (value){
          
                setState(() {
                  _telefono = value; 
                });
              },
            )
        );       
  }

  Widget _crearInputMonto() {
    
      return Container(
        width: MediaQuery.of(context).size.width*1,
        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 0.0, bottom: 0.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color: Colors.white,
          boxShadow: [BoxShadow(blurRadius: 10.0, color: Colors.black12)]
        ),
        child: TextFormField(
            keyboardType: TextInputType.number ,
            inputFormatters: <TextInputFormatter>[
              WhitelistingTextInputFormatter.digitsOnly
            ],
            decoration: InputDecoration(
              border: InputBorder.none,
              labelText: 'Monto',
              hintText: 'Digita el monto a transferir',
              icon: Icon(FontAwesomeIcons.coins)
            ),
            onChanged: (value){
              
              setState(() {
                _monto = value; 
              });
            },
            validator: (value) {
              if(value == ''){
                return 'Debe ingresar un monto.';
              }
              else if ( int.parse(value) == 0) {
                return 'Debe ser un monto mayor a 0.';
              }
              return null;
            },
        ),
    );
  }

  Widget _crearInputConcepto() {
      return Container(
        width: MediaQuery.of(context).size.width*1,
        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 0.0, bottom: 0.0),
        decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        color: Colors.white,
        boxShadow: [BoxShadow(blurRadius: 10.0, color: Colors.black12)]),
        child: TextFormField(
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          border: InputBorder.none,
          labelText: 'Descripción',
          hintText: 'Digita la descripción',
          icon: Icon(Icons.message)
        ),
        onChanged: (value){
          
          setState(() {
            _concepto = value; 
          });
        },
        validator: (value) {
          if (value.isEmpty) {
            return 'Debe incluir el concepto para pasar dinero.';
          }
          return null;
        },
    ),
      );
  }

  void _enviarMensaje(){
     String message = "PASE " + _monto+" "+ _telefono +" "+_concepto;

    sendSMS(message);
    Navigator.pop(context);
  }

  invocarContactos() async{
   
    PermissionStatus permissionStatus = await _getContactPermission();
    if (permissionStatus == PermissionStatus.granted) {
      Contact contact = await _contactPicker.selectContact();         

      _telefono = '';
      _telefono = contact.phoneNumber.number.toString().replaceAll(new RegExp(r"\s+\b|\b\s"), "");
      _telefono = _telefono.replaceAll(new RegExp(r"-"), "");
      var finIndex = _codigoPais.length;

      if(_codigoPais == _telefono.substring(0,finIndex)){
        _telefono = _telefono.substring(finIndex, _telefono.length);
      }

      setState(() {
        phoneTextFieldController.text = '';
        phoneTextFieldController.text = _telefono;     
      });
    } else {
      _handleInvalidPermissions(permissionStatus);
    }
  

 }
  

invocarContactosiOS() async{
   
      Contact contact = await _contactPicker.selectContact();         

      _telefono = '';
      _telefono = contact.phoneNumber.number.toString().replaceAll(new RegExp(r"\s+\b|\b\s"), "");
      _telefono = _telefono.replaceAll(new RegExp(r"-"), "");
      var finIndex = _codigoPais.length;

      if(_codigoPais == _telefono.substring(0,finIndex)){
        _telefono = _telefono.substring(finIndex, _telefono.length);
      }

      setState(() {
        phoneTextFieldController.text = '';
        phoneTextFieldController.text = _telefono;     
      });
  

 }

  // Asking Contact permissions
  Future<PermissionStatus> _getContactPermission() async {
    PermissionStatus permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.contacts);
    if (permission != PermissionStatus.granted && permission != PermissionStatus.disabled) {
      Map<PermissionGroup, PermissionStatus> permissionStatus = await PermissionHandler().requestPermissions([PermissionGroup.contacts]);
      return permissionStatus[PermissionGroup.contacts] ?? PermissionStatus.unknown;
    } else {
      return permission;
    }
  }

  // Managing error when you don't have permissions
  void _handleInvalidPermissions(PermissionStatus permissionStatus) {
    if (permissionStatus == PermissionStatus.denied) {
      throw new PlatformException(
          code: "PERMISSION_DENIED",
          message: "Access to location data denied",
          details: null);
    } else if (permissionStatus == PermissionStatus.disabled) {
      throw new PlatformException(
          code: "PERMISSION_DISABLED",
          message: "Location data is not available on device",
          details: null);
    }
  }

  Future<Null> _mostrarContactosFavoritos(BuildContext context) async{
   List<Contacto> favoriteElements = [];
   
   final InputDecoration searchDecoration = const InputDecoration();

   _contactosFavoritos = await DBProvider.db.obtenerTodosContactos();
        
    if (_contactosFavoritos != null)
    { 
      showDialog(
        context: context,
        builder: (_) =>
            FavoritoDialogo(
              _contactosFavoritos,
              favoriteElements,
              showCountryOnly: false,
              emptySearchBuilder: null,
              searchDecoration: searchDecoration,
            ),
      ).then((e) {
        if (e != null) {
          setState(() {
            _contactoActual = e;
          });
          _telefono = '';
          phoneTextFieldController.text = '';
          guestnameTextFieldController.text = _contactoActual.nombre;
          _telefono = _contactoActual.telefono;
          var finIndex = _codigoPais.length;

          if(_codigoPais == _telefono.substring(0,finIndex)){
            _telefono = _telefono.substring(finIndex, _telefono.length);
          }
          phoneTextFieldController.text = _telefono;

        }
      });
    
    }
   
  }

  _crearLeading() {
    if(Platform.isAndroid){
      return IconButton(
        color: Colors.white,
        icon: Icon(Icons.arrow_back),
        onPressed: () => Navigator.of(context).pop(),
      );
    }
    else{
      return IconButton(
        color: Colors.white,
        icon: Icon(CupertinoIcons.back),
        onPressed: () => Navigator.of(context).pop(),
      );
    }
      
       
  }

  _crearBottomNavigationBar() {
    if(Platform.isAndroid){
      return BottomAppBar(                
                child: Container(
                  height: 50.0,

                ),
              );
    }
    else{
      return BottomNavigationBar(
       currentIndex: 0, // this will be set when a new tab is tapped
       items: [
         BottomNavigationBarItem(
           icon: new Icon(Icons.person),
           title: new Text('Contactos'),
         ),
         BottomNavigationBarItem(
           icon: new Icon(Icons.star_border),
           title: new Text('Favoritos'),
         )
         
       ],
       onTap: (selected){
         switch(selected){
           case 0: invocarContactosiOS(); break;
           case 1: _mostrarContactosFavoritos(context); break;
           default: invocarContactosiOS(); break;
         }
       },
     );
    }
  }

  _crearFloatingActionButtons() {
    if(Platform.isAndroid){
      return FloatingButtons((){
          invocarContactos();                
        },() async{
          _mostrarContactosFavoritos(context);
        }
      );
    }

  }

}