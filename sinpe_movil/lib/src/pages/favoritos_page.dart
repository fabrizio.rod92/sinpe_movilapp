import 'dart:convert';
import 'dart:io';

import 'package:contact_picker/contact_picker.dart';
//import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:sinpe_movil/src/models/contact_model.dart';
import 'package:sinpe_movil/src/pages/pase_dinero.dart';
import 'package:sinpe_movil/src/pages/recarga_page.dart';
import 'package:sinpe_movil/src/providers/db_provider.dart';
import 'package:toast/toast.dart';

 
class FavoritosPage extends StatefulWidget {
  FavoritosPage({Key key}) : super(key: key);

  _FavoritosPageState createState() => _FavoritosPageState();
}

class _FavoritosPageState extends State<FavoritosPage> {
  final ContactPicker _contactPicker = new ContactPicker();

  final String _codigoPais = "+506";
  String _nombre = '';
  String _telefono = '';
   
  ProgressDialog pr;

  TextEditingController filterController = new TextEditingController();
  String filter;
  
  List<Contacto> _contactos = List();
  List<Contacto> _contactosFiltrados = List();

  @override
  void initState() {   
    super.initState();     
    filterController.addListener(() {
      setState(() {
        filter = filterController.text;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(context,type:ProgressDialogType.Normal);
    pr.style(
      message: "Cargando"
    );

    return Scaffold(      
      appBar: AppBar(
        actions: <Widget>[
          _crearTrailingiOS()
        ],
        leading: _crearLeading(),
        backgroundColor: Color(0xFF1b1e24),
        brightness: Brightness.dark,
        title: Text(
          'Contactos favoritos',
          style: TextStyle(
            color: Colors.white
          )
          
        ),      
      ),
      body: Column(
        children: <Widget>[
          _crearInputBusqueda(),
          Expanded(
              child: _lista(),          
          )
        ],
      ),
      bottomNavigationBar: _crearBottomNavigationBar(),
      floatingActionButton: _crearFloatingActionButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

    );
  }

  Widget _crearInputBusqueda() {
    return 
        Container(
            width: MediaQuery.of(context).size.width*1,
            padding:
            EdgeInsets.only(left: 10.0, right: 10.0, top: 0.0, bottom: 0.0),
            margin:EdgeInsets.only(left: 10.0, right: 10.0, top: 20.0, bottom: 20.0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
                boxShadow: [BoxShadow(blurRadius: 10.0, color: Colors.black12)]),
            child: TextFormField(
              controller: filterController,
              decoration: new InputDecoration(
                  border: InputBorder.none,
                  icon: Icon(Icons.search),
                  labelText: 'Buscar contacto favorito',
                  hintText: 'Digita el nombre o número de teléfono',
                  suffixStyle: const TextStyle(color: Colors.green)
              ),
            )
        );       
  }


  Widget _lista() {    

      return FutureBuilder(
          key: UniqueKey(),
          future:  Future.delayed(Duration(milliseconds: 500),(){
            return  DBProvider.db.obtenerTodosContactos();
          }),
          initialData: [],
          builder: (context, AsyncSnapshot<List<dynamic>> snapshot){
            if (snapshot.hasData) {            
            _contactos = contactosFromJson(snapshot.data);
            //_contactos = snapshot.data;
            _contactosFiltrados = _contactos;

            return ListView.builder(
              key: UniqueKey(),
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                
                return filter == null || filter == "" ? 
                _crearListTileContacto(_contactosFiltrados[index]) : 
                _contactosFiltrados[index].nombre.toLowerCase().contains(filter.toLowerCase()) ||
                _contactosFiltrados[index].telefono.toLowerCase().contains(filter.toLowerCase())  ? 
                _crearListTileContacto(_contactosFiltrados[index]) : Container();
                //return _crearListTileContacto(item);
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
            
          },
      );
  }
  
  Widget _crearListTileContacto(Contacto item){
    return Dismissible(
          direction: DismissDirection.endToStart,                      
          key: UniqueKey(),
          background: Container(   
            height: 20.0,
            alignment: AlignmentDirectional.centerEnd,                    
            child: Padding(
              padding: EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
              child: Icon(Icons.delete_forever,color: Colors.white38,size: 40.0,),
            ),
            color: Colors.red,
          ),
          onDismissed: (direction){
            DBProvider.db.borrarContacto(item.id).then((resultado){
              if(resultado > 0){
              Toast.show("Contacto eliminado", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
              }
              else{
                Toast.show("No se pudo eliminar", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
              }
            });
          },
          child: InkWell(
            onTap: (){},
            onLongPress: (){},
            child: Container(  
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),                                               
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  blurRadius: 10.0,
                  color: Colors.black26,
                  spreadRadius: 1.0,
                  offset: Offset(2.0,10.0)
                )
              ]
            ),
            child: ListTile(
              key: Key(item.id.toString()),
              title: Text(item.nombre),
              subtitle: Text(item.telefono),
              leading: Icon(Icons.account_circle,size: 40.0,),
              onTap:(){                                
                _mostrarDialogoOpciones(context, item);
              },                            
            ),
        ),
      )
    );
  }

  void _mostrarDialogoOpciones(BuildContext context, Contacto item) {

  if(Platform.isIOS){
    showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
      actions: <Widget>[
        CupertinoActionSheetAction(
          child: Text('Pasar dinero'),
          onPressed:(){
                  Navigator.push(
                    context, 
                    MaterialPageRoute(
                      builder: (context) => PaseDineroPage(numero: item.telefono,)
                    )
                  );
                },
        ),
        
      ],
      cancelButton: CupertinoActionSheetAction(
          child: Text('Cancelar',style: TextStyle(color: Colors.red),),
          onPressed:(){
              Navigator.pop(context,'Cancel');
          },
        ),
    )
    );
    
  }
  else{
  showModalBottomSheet(                                
        context: context,
        builder:(BuildContext context){
          return Column(   
            mainAxisAlignment: MainAxisAlignment.center,   
            mainAxisSize: MainAxisSize.min,                                
            children: <Widget>[
              ListTile(
                title: Text('Pasar dinero'),
                onTap: (){
                  Navigator.push(
                    context, 
                    MaterialPageRoute(
                      builder: (BuildContext context) => PaseDineroPage(numero: item.telefono,)
                    )
                  );
                },
                trailing: Icon(Icons.chevron_right),
              ),
              Divider(),
              ListTile(
                title: Text(
                  'Cancelar',
                  style: TextStyle(color: Colors.red),
                  textAlign: TextAlign.center,
                ),
                onTap: (){
                  Navigator.pop(context);
                  
                }
              )
            ],
          );
        } 
      );
  }
    
  }
 
 invocarContactos() async{
   
    PermissionStatus permissionStatus = await _getContactPermission();
    if (permissionStatus == PermissionStatus.granted) {
      Contact contact = await _contactPicker.selectContact();          

      _telefono = '';
      _nombre = '';
      _telefono = contact.phoneNumber.number.toString().replaceAll(new RegExp(r"\s+\b|\b\s"), "");
      _telefono = _telefono.replaceAll(new RegExp(r"-"), "");
      _nombre = contact.fullName.toString() + ' ' + contact.phoneNumber.label;
      var finIndex = _codigoPais.length;

      if(_codigoPais == _telefono.substring(0,finIndex)){
        _telefono = _telefono.substring(finIndex, _telefono.length);
      }

      setState((){
         Contacto nuevo = new Contacto(
            nombre: _nombre,
            telefono: _telefono 
          );
         DBProvider.db.nuevoContacto(nuevo).then((resultado){
            if(resultado > 0){
              Toast.show("Contacto agregado", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
            }
            else{
              Toast.show("Contacto ya existe", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
            }
         });
         
      });
    } else {
      _handleInvalidPermissions(permissionStatus);
    }
  

 }

 invocarContactosiOS() async{
   
      Contact contact = await _contactPicker.selectContact();          

      _telefono = '';
      _nombre = '';
      _telefono = contact.phoneNumber.number.toString().replaceAll(new RegExp(r"\s+\b|\b\s"), "");
      _telefono = _telefono.replaceAll(new RegExp(r"-"), "");
      _nombre = contact.fullName.toString() + ' ' + contact.phoneNumber.label;
      var finIndex = _codigoPais.length;

      if(_codigoPais == _telefono.substring(0,finIndex)){
        _telefono = _telefono.substring(finIndex, _telefono.length);
      }

      setState((){
         Contacto nuevo = new Contacto(
            nombre: _nombre,
            telefono: _telefono 
          );
         DBProvider.db.nuevoContacto(nuevo).then((resultado){
            if(resultado > 0){
              Toast.show("Contacto agregado", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
            }
            else{
              Toast.show("Contacto ya existe", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
            }
         });
         
      });
  

 }

  // Asking Contact permissions
  Future<PermissionStatus> _getContactPermission() async {
    PermissionStatus permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.contacts);
    if (permission != PermissionStatus.granted && permission != PermissionStatus.disabled) {
      Map<PermissionGroup, PermissionStatus> permissionStatus = await PermissionHandler().requestPermissions([PermissionGroup.contacts]);
      return permissionStatus[PermissionGroup.contacts] ?? PermissionStatus.unknown;
    } else {
      return permission;
    }
  }

  // Managing error when you don't have permissions
  void _handleInvalidPermissions(PermissionStatus permissionStatus) {
    if (permissionStatus == PermissionStatus.denied) {
      throw new PlatformException(
          code: "PERMISSION_DENIED",
          message: "Access to location data denied",
          details: null);
    } else if (permissionStatus == PermissionStatus.disabled) {
      throw new PlatformException(
          code: "PERMISSION_DISABLED",
          message: "Location data is not available on device",
          details: null);
    }
  }

  _crearLeading() {
    if(Platform.isAndroid){
      return IconButton(
        color: Colors.white,
        icon: Icon(Icons.arrow_back),
        onPressed: () => Navigator.of(context).pop(),
      );
    }
    else{
      return IconButton(
        color: Colors.white,
        icon: Icon(CupertinoIcons.back),
        onPressed: () => Navigator.of(context).pop(),
      );
    }  
  }

  _crearBottomNavigationBar() {
    if(Platform.isAndroid){
      return BottomAppBar(                
        child: Container(
          height: 50.0,
        ),
      );
    }    
    
  }

  _crearFloatingActionButton() {
    if(Platform.isAndroid){
      return FloatingActionButton(
        heroTag: 'btnContactos',
        onPressed: invocarContactos,
        child: Icon(Icons.add),
        backgroundColor: Color(0xFF075B9A),
      );
    }
  }

  Widget _crearTrailingiOS() {
    if(Platform.isIOS){
        return IconButton(
        icon: Icon(Icons.add),
        color: Color(0xFF075B9A),
        onPressed: () async{
          invocarContactosiOS();
        },
      );
    }

    return Container();
  }

  
}

