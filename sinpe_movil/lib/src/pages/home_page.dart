import 'dart:ui' as prefix0;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:sinpe_movil/src/providers/menu_provider.dart';
import 'package:sinpe_movil/src/utils/icono_string_util.dart';
  
class HomePage extends StatelessWidget {

  
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
          onWillPop: (){
            _showDialog(context);
          },
          child: Scaffold(     
        appBar: AppBar(                 
          automaticallyImplyLeading: false,      
          title: Text('Sinpe Móvil',style: TextStyle(color: Colors.white),),
          backgroundColor: Color(0xFF1b1e24),   
          brightness: Brightness.light,
          actions: <Widget>[          
            Image(image: AssetImage('assets/img/bdpel_white.png'),)
          ],          
        ),
        body: Center(child: _lista()),
      
      ),
    );
  }

  Widget _lista() {    

      return FutureBuilder(
          future: menuProvider.cargarData(),
          initialData: [],
          builder: (context, AsyncSnapshot<List<dynamic>> snapshot){
            
            return ListView(
                shrinkWrap: true,
                padding: EdgeInsets.all(20.0),
                children: [
                  Center(                
                    child: Column(children: _opcionTarjeta(snapshot.data, context),),
                  ),
                ]
            );
          },
      );
  }

  
   List<Widget> _opcionTarjeta(List<dynamic> data, BuildContext context) {
    final List<Widget> widgets = [];

    data.forEach((opt){
      final card = InkWell(
        onTap: (){
          Navigator.pushNamed(context, opt['ruta']);
        },
        child: Container(
          padding: EdgeInsets.all(10.0),      
          height: MediaQuery.of(context).size.height * 0.14,      
          child: Row(        
            children: <Widget>[
              Container(            
                child:Center(
                    child: CircleAvatar(
                    backgroundColor: Color(int.parse(opt['circlecolor'])),
                    radius: 35.0,
                    child: getIcon(opt['icon'], color: Color(int.parse(opt['iconcolor']))),
                  ),
                ),
              ),
              Container(
                width:MediaQuery.of(context).size.width * 0.5,            
                padding: EdgeInsets.all(10.0),
                margin: EdgeInsets.only(left: 10.0),
                child: Text(
                    opt['texto'],
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0
                    ),
                  ),
                
              )
            ],
          ),
        )
      ); 
      
      final container = Container(          
          decoration: BoxDecoration(
            color: Color(int.parse(opt['cardcolor'])),
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: <BoxShadow>[
              BoxShadow(
                blurRadius: 10.0,
                color: Colors.black26,
                spreadRadius: 2.0,
                offset: Offset(2.0,10.0)
              )
            ],
          ),
          child: ClipRRect(
            child: card,
            borderRadius: BorderRadius.circular(20.0),
          )
      );

      widgets..add(container)
            ..add(SizedBox(height: 20.0,));

    });

  return widgets;

  }

  void _showDialog(BuildContext context) {
    // flutter defined function
    showDialog(
      context: context,      
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text("Cerrar aplicación",textAlign: TextAlign.center,),
          content: Text('¿Desear cerrar Sinpe Móvil?', textAlign: TextAlign.center,),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20.0))),
          actions: <Widget>[            
                FlatButton(              
                  child: Text('No'),

                  onPressed: (){
                    Navigator.of(context).pop();
                  }
                ),
                Divider(),
                FlatButton(
                  child: Text("Sí"),
                  onPressed: () {
                    SystemNavigator.pop();
                  },
                ),
          ],            
        );
      },
    );
  }

}