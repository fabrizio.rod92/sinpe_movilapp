import 'package:flutter/material.dart';
import 'package:sinpe_movil/src/utils/send_message_util.dart';


class UltimosMovsPage extends StatelessWidget {
 
  UltimosMovsPage(BuildContext context){
    String message = "ULTIMOS";

    sendSMS(message);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ultimos Movimientos'),
      ),
    );
  }
}