import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sinpe_movil/src/models/contact_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

class DBProvider {
  DBProvider._();
  static final DBProvider db = DBProvider._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null)
    return _database;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "sinpe.db");
    return await openDatabase(path, version: 1, onOpen: (db) {
    }, onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE Contacto ("
          "id INTEGER PRIMARY KEY,"
          "nombre TEXT,"
          "telefono TEXT);"
      );
    });
  }

  nuevoContacto(Contacto nuevoContacto) async {
    final db = await database;
    var res = await db.rawInsert(
      "INSERT Into Contacto (nombre,telefono)"
      " VALUES ('${nuevoContacto.nombre}','${nuevoContacto.telefono}')");
    return res;
  }

  obtenerContacto(String nombre) async {
    final db = await database;
    var res =await  db.query("Contacto", where: "nombre = ?", whereArgs: [nombre]);
    return res.isNotEmpty ? Contacto.fromMap(res.first) : Null ;
  }

   Future<List<Contacto>> obtenerTodosContactos() async {
    final db = await database;
    var res = await db.query("Contacto");
    List<Contacto> list =
        res.isNotEmpty ? res.map((c) => Contacto.fromMap(c)).toList() : [];
    return list;
  }

  actualizarcontacto(Contacto nuevoContacto) async {
    final db = await database;
    var res = await db.update("Contacto", nuevoContacto.toMap(nuevoContacto),
        where: "id = ?", whereArgs: [nuevoContacto.id]);
    return res;
  }

  borrarContacto(int id) async {
    final db = await database;
    db.delete("Contacto", where: "id = ?", whereArgs: [id]);
  }

  borrarTodo() async {
    final db = await database;
    db.rawDelete("Delete * from Contacto");
  }
}