import 'dart:convert';

Contacto clientFromJson(String str) {
    final jsonData = json.decode(str);
    return Contacto.fromJson(jsonData);
}

List<Contacto> contactosFromJson(List<dynamic> data){
  List<Contacto> contactos = List();

  data.forEach((contacto){    
     Contacto nuevoContacto = clientFromJson(json.encode(contacto));
     contactos.add(nuevoContacto);
  });


  return contactos;
}

String clientToJson(Contacto data) {
    final dyn = data.toJson();
    return json.encode(dyn);
}

class Contacto {
    int id;
    String nombre;
    String telefono;

    Contacto({
        this.id,
        this.nombre,
        this.telefono,
    });

    factory Contacto.fromJson(Map<String, dynamic> json) => new Contacto(
        id: json["id"],
        nombre: json["nombre"],
        telefono: json["telefono"]
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
        "telefono": telefono,
    };

    factory Contacto.fromMap(Map<String,dynamic> response) => new Contacto(
      id: response['id'],
      nombre: response['nombre'],
      telefono: response['telefono']
    );

     Map<String, dynamic> toMap(Contacto contacto) => {
        "id": contacto.id,
        "nombre": contacto.nombre,
        "telefono": contacto.telefono,
     };
        
    
        
    
}