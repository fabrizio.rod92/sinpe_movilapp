import 'package:flutter/material.dart';
import 'package:sinpe_movil/src/pages/pase_dinero.dart';
import 'package:sinpe_movil/src/routes/routes.dart';
import 'package:sinpe_movil/src/utils/portrait_mixin.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget with PortraitModeMixin {
  
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Sinpe Móvil',
      initialRoute: '/',
      routes: getApplicationRoutes(),
      onGenerateRoute: (settings){
        return MaterialPageRoute(
          builder: (BuildContext context) => PaseDineroPage()
        );
      },
    );
  }
}