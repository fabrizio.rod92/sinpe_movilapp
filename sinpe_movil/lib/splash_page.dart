import 'package:flutter/material.dart';
import 'package:sinpe_movil/src/pages/home_page.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key key}) : super(key: key);

  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

  @override
  void initState() {
    super.initState();
    Future.delayed(
      Duration(seconds: 2),
      (){
        Navigator.push(
          context, 
          MaterialPageRoute(
            builder: (BuildContext context) => HomePage()
          )
        );
      }
      
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       body: Stack(
         children: <Widget>[
           Container(
             decoration: BoxDecoration(color: Colors.white),
           ),
           Column(
             mainAxisAlignment: MainAxisAlignment.center,
             children: <Widget>[               
              Expanded(
                flex: 2,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image(
                        image: AssetImage('assets/img/bdpel.png'),
                      )
                    ],
                  ),
                ),
              )
            ],
           )
         ],
       ),
    );
  }
}